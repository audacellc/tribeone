$('.nav-toggle').click(function(){
  $('.header .header-nav ul').toggleClass('show');
  $(".nav-toggle span").toggleClass("nav-active");
  $(this).toggleClass('show')
})

$(".header").addClass("header-0");

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.querySelector("header").style.top = "0";
  } else {
    document.querySelector("header").style.top = "-114px";
  }
  prevScrollpos = currentScrollPos;
}


// header
$(document).ready(function(){
  $(window).scroll(function() {
    if($(this).scrollTop() > 50){
      $(".header").removeClass("header-0");
      $(".header").addClass('header-active');
      $(".scroll-menu").addClass("header-active-link");
      $(".top-header-logo").addClass("top-header-logo-active");
      $("#top-header-logo").attr("src", "./images/tribeone-logo-blue.png");
      $(".nav-toggle span").addClass("nav-active");
      $(".bsc-bridge-btn").addClass("contact-button-active");
    }
    else {
      $("#top-header-logo").attr("src", "./images/tribe-logo-white.png");
      $('.header').removeClass('header-active');
      $(".scroll-menu").removeClass("header-active-link");
      $(".top-header-logo").removeClass("top-header-logo-active");
      $(".nav-toggle span").removeClass("nav-active");
      $(".bsc-bridge-btn").removeClass("contact-button-active");
    }
  });
})

$(".mobile-investment-swiper").addClass("show-swiper");
var mySwiper = new Swiper('.mobile-investment-swiper', {
  slidesPerView: 1,
  spaceBetween: 0,
  effect: 'fade',
  centeredSlides: true,
  pagination: {
    el: '.investment-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.investment-app-next-button',
    prevEl: '.investment-app-prev-button',
  },
  autoplay : false,
});


$(".app-section").addClass("rotated-bg");


$("#borrowApp").click(function(){
  mySwiper = new Swiper('.mobile-investment-swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    effect: 'fade',
    centeredSlides: true,
    pagination: {
      el: '.investment-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.investment-app-next-button',
      prevEl: '.investment-app-prev-button',
    },
    autoplay : false,
  });
  // $(".mobile-investment-swiper").removeClass("show-swiper");
  // $(".mobile-borrow-swiper").addClass("show-swiper");
  $("#swipe-img-1").attr("src", "./images/borrow/screen-01.png");
  $("#swipe-img-2").attr("src", "./images/borrow/screen-02.png");
  $("#swipe-img-3").attr("src", "./images/borrow/screen-03.png");
  $("#swipe-img-4").attr("src", "./images/borrow/screen-04.png");
  $("#swipe-img-5").attr("src", "./images/borrow/screen-05.png");
  $("#swipe-img-6").attr("src", "./images/borrow/screen-06.png");
  $("#swipe-img-7").attr("src", "./images/borrow/screen-07.png");
  $(".app-section").addClass("rotated-bg");
  mySwiper.updateSlides();
})

$("#investmentApp").click(function(){
  // $(".mobile-borrow-swiper").removeClass("show-swiper");
  // $(".mobile-investment-swiper").addClass("show-swiper");
  mySwiper = new Swiper('.mobile-investment-swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    effect: 'fade',
    centeredSlides: true,
    pagination: {
      el: '.investment-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.investment-app-next-button',
      prevEl: '.investment-app-prev-button',
    },
    autoplay : false,
  });
  $("#swipe-img-1").attr("src", "./images/lend/screen-01.png");
  $("#swipe-img-2").attr("src", "./images/lend/screen-02.png");
  $("#swipe-img-3").attr("src", "./images/lend/screen-03.png");
  $("#swipe-img-4").attr("src", "./images/lend/screen-04.png");
  $("#swipe-img-5").attr("src", "./images/lend/screen-05.png");
  $("#swipe-img-6").attr("src", "./images/lend/screen-06.png");
  $(".app-section").removeClass("rotated-bg");
  mySwiper.updateSlides();
})

$(".switch-borrow").click(function(){
  $(".product-section").addClass("rotated-bg");
})

$(".switch-investment").click(function(){
  $(".product-section").removeClass("rotated-bg");
})
// $(".loan-card-container").addClass("show-card-container");
$(".borrow-product-card").addClass("show-card-container");

$(".switch-label-on").click(function(){
  $(".borrow-product-card").removeClass("show-card-container");
  $(".loan-card-container").addClass("show-card-container");
  $(".product-card").addClass("show-card-container");
})
$(".switch-label-off").click(function(){
  $(".product-card").removeClass("show-card-container");
  $(".investment-card-container").addClass("show-card-container");
  $(".borrow-product-card").addClass("show-card-container");
})



// raodmap slide
const rightBtn = document.querySelector('#timeline-right-button');
const leftBtn = document.querySelector('#timeline-left-button');
rightBtn.addEventListener("click", function(event) {
  const conent = document.querySelector('.roadmap-continer');
  conent.scrollLeft += 300;
  event.preventDefault();
});

leftBtn.addEventListener("click", function(event) {
  const conent = document.querySelector('.roadmap-continer');
  conent.scrollLeft -= 300;
  event.preventDefault();
});

const slider = document.querySelector('.roadmap-continer');
let isDown = false;
let startX;
let scrollLeft;

slider.addEventListener('mousedown', (e) => {
  isDown = true;
  slider.classList.add('active');
  startX = e.pageX - slider.offsetLeft;
  scrollLeft = slider.scrollLeft;
});
slider.addEventListener('mouseleave', () => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mouseup', () => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mousemove', (e) => {
  if(!isDown) return;
  e.preventDefault();
  const x = e.pageX - slider.offsetLeft;
  const walk = (x - startX) * 2; //scroll-fast
  slider.scrollLeft = scrollLeft - walk;
});

var flkty = new Flickity( '.main-carousel', {
  wrapAround: true,
  groupCells : 3,
  cellAlign : "center",
});
var swiper = new Swiper('.text-slider', {
  direction: 'vertical',
  loop: true,
  autoplay: {
    delay: 1500,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: false,
  },
});











// var partnerSwiper = new Swiper('.partner-carousel', {
//    // Default parameters
//   slidesPerView: 2,
//   spaceBetween: 10,
//   intialSlide : 5,
//   centeredSlides: true,
//   roundLengths: true,
//   loop : true,
//   pagination: {
//     el: '.swiper-pagination',
//     type: 'bullets',
//     clickable : true,
//   },
//   navigation: {
//     nextEl: '.partner-button-next',
//     prevEl: '.partner-button-prev',
//   },
//    breakpoints: {
//      // when window width is >= 320px
//     270: {
//       slidesPerView: 2,
//       spaceBetween: 20,
//       slidesPerGroup : 2,
//     },
//     480: {
//       slidesPerView: 2,
//       spaceBetween: 20,
//       slidesPerGroup : 2,
//     },
//     640: {
//       slidesPerView: 4,
//       spaceBetween: 30
//     },
//     992 : {
//       slidesPerView: 4,
//       spaceBetween: 30,
//       slidesPerGroup: 2,
//       loop : true
//     },
//     1366 : {
//       slidesPerView: 5,
//       spaceBetween: 20,
//       slidesPerGroup: 3,
//       loop : true
//     }
//    }
// });
// swiper.slideTo(0, true,true);




















// old code
//  new WOW().init();


// if($(window).width() <= 991){
//  $('.our-partner ul').owlCarousel({
//      loop:true,
//      margin:0,
//      nav:false,
//      dots:true,
//      items:1,
//      responsive:{
//          0:{
//              items:2
//          },
//          600:{
//              items:3
//          },
//          991:{
//              items:4
//          }
//      }
//  })
// }