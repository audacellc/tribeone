$(".contact-us-button").click(function(){
  $(".contact-popup").addClass("show-popup");
})
$(".popup-close-icon").click(function(){
  $(".contact-popup").removeClass("show-popup");
  $('#submit').attr('disabled', 'disabled'); 
  $("#name").val("");
  $("#email").val("");
  $("#telegramHandle").val("");
  $("#questions").val("");
  $("#allocation").val("");
})




var contactData = {
  name : "",
  email : "",
  telegramHandle : "",
  questions : "",
  allocationAmount : "",
  interestedInSale : false,
  from : "tribe_one"
}

var isValid = {
  name : false,
  email : false,
  telegramHandle : false,
  questions : false,
  allocationAmount : false,
  interestedInSale : false,
  checked : false,
}

let messageType = "";
var isValidName = true;
$(".error-massage").addClass("show-error");


$("#name").keyup(function(){
  var name = $(this).val();
  contactData.name = $(this).val();
  if(name !== ""){
    isValid.name = true;
    isValidName  = true;
    $(".name-error").addClass("show-error");
  }
  else{
    isValid.name = false;
    isValidName  = false;
    $(".name-error").removeClass("show-error");
  }
  isValidData();
})


$("#questions").keyup(function(){
  var questions = $(this).val();
  contactData.questions = $(this).val();
  if(questions !== ""){
    isValid.questions = true;
    $(".question-error").addClass("show-error");
  }
  else{
    isValid.questions = false;
    $(".question-error").removeClass("show-error");
  }
  isValidData();
})

$("#allocation").keyup(function(){
  var allocationAmount = $(this).val();
  contactData.allocationAmount = $(this).val();
  if(allocationAmount !== ""){
    isValid.allocationAmount = true;
    $(".allocation-error").addClass("show-error");
  }
  else{
    isValid.allocationAmount = false;
    $(".allocation-error").removeClass("show-error");
  }
  isValidData();
})

$("#email").keyup(function(){
  const email = $(this).val();
  contactData.email = $(this).val();
  const atPos = email.indexOf("@");
  const dotPos = email.lastIndexOf(".");
  if(email === ""){
    isValid.email = false;
    $(".email-error").removeClass("show-error");
  }
  else{
    isValid.email = true;
    $(".email-error").addClass("show-error");
  }
  isValidData();
})



$("#telegramHandle").click(function(){
  $(this).val("@");
})
$("#telegramHandle").keyup(function(){
  const telegram = $(this).val();
  contactData.telegramHandle = $(this).val();
  const atPos = telegram.indexOf("@");
  if(telegram.length < 2){
    isValid.telegramHandle = false;
    $(".tg-error").removeClass("show-error");
  }
  else{
    isValid.telegramHandle = true;
    $(".tg-error").addClass("show-error");
  }
  isValidData();
})
function recaptchaCallback() {
  isValid.checked = true;
  isValidData();
};


var interestedPrivateSale = undefined;

$("#allocation").css("display", "none");
$("#interest-yes").click(function(){
  $("#allocation").css("display", "block");
  interestedPrivateSale = true;
  isValid.interestedInSale = true;
  contactData.interestedInSale = true;
  // isValidData();
})

$("#interest-no").click(function(){
  $("#allocation").css("display", "none");
  interestedPrivateSale = false;
  isValid.interestedInSale = false;
  contactData.interestedInSale = false;
  isValidData();
})


function isValidData() {
  if(
    isValid.name && 
    isValid.email && 
    isValid.telegramHandle && 
    isValid.questions )
    // isValid.checked
    // && ((isValid.interestedInSale && isValid.allocationAmount ) || (interestedPrivateSale !== undefined && isValid.interestedInSale === false) ))
  {
    $('#submit').removeAttr('disabled');
  }
  else{
    $('#submit').attr('disabled', 'disabled'); 
  }
}

$("#contact-form").submit(function(e){
  e.preventDefault();
  $("#submit").attr('disabled', 'disabled');
  $("#submit").text("Sending . . .");
  const mailUrl = "https://travel-api.971insurance.ae/send-uno-mail";
  $.ajax({
    url : mailUrl,
    type : "POST",
    headers : {
      "Content-Type" : 'application/json'
    },
    data : JSON.stringify({...contactData, messageType}),
    success : function(res){
      $("#submit").text("SUBMIT");
      $(".contact-popup").removeClass("show-popup");
      $("#name").val("");
      $("#email").val("");
      $("#telegramHandle").val("");
      $("#questions").val("");
      $("#allocation").val("");
      $("#thanks-popup").addClass("show-popup");
    }
  })
})

$(".contact-us-button").click(function(){
  messageType = "contact_us";
  $(".contact-popup").addClass("show-popup");
})
// $(".register-button").click(function(){
//   messageType = "register_interest";
//   $(".contact-popup").addClass("show-popup");
// })


$("#thanks-btn").click(function(){
  $(".thanks-container").removeClass("show-popup");
})
