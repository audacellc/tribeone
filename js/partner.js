$("#NFT-Marketplace-tab").addClass("partner-type-active");
$("#NFT-Marketplace").addClass("show-partner-container");

$("#NFT-Marketplace-tab").click(function(){
  $("#liquidity-platform-tab").removeClass("partner-type-active");
  $("#liquidity-platform").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#infrastructure-tab").removeClass("partner-type-active");
  $("#infrastructure").removeClass("show-partner-container");
  $("#integration-tab").removeClass("partner-type-active");
  $("#integration").removeClass("show-partner-container");
  $("#insurance-protocol-tab").removeClass("partner-type-active");
  $("#insurance-protocol").removeClass("show-partner-container");
  $("#exchange-partners-tab").removeClass("partner-type-active");
  $("#exchange-partners").removeClass("show-partner-container");
  $("#NFT-Marketplace-tab").addClass("partner-type-active");
  $("#NFT-Marketplace").addClass("show-partner-container");
});

$("#liquidity-platform-tab").click(function(){
  $("#NFT-Marketplace-tab").removeClass("partner-type-active");
  $("#NFT-Marketplace").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#infrastructure-tab").removeClass("partner-type-active");
  $("#infrastructure").removeClass("show-partner-container");
  $("#integration-tab").removeClass("partner-type-active");
  $("#integration").removeClass("show-partner-container");
  $("#insurance-protocol-tab").removeClass("partner-type-active");
  $("#insurance-protocol").removeClass("show-partner-container");
  $("#exchange-partners-tab").removeClass("partner-type-active");
  $("#exchange-partners").removeClass("show-partner-container");
  $("#liquidity-platform-tab").addClass("partner-type-active");
  $("#liquidity-platform").addClass("show-partner-container");
});

// $("#platform-tab").click(function(){
//   $("#NFT-Marketplace-tab").removeClass("partner-type-active");
//   $("#NFT-Marketplace").removeClass("show-partner-container");
//   $("#liquidity-platform-tab").removeClass("partner-type-active");
//   $("#liquidity-platform").removeClass("show-partner-container");
//   $("#infrastructure-tab").removeClass("partner-type-active");
//   $("#infrastructure").removeClass("show-partner-container");
//   $("#integration-tab").removeClass("partner-type-active");
//   $("#integration").removeClass("show-partner-container");
//   $("#insurance-protocol-tab").removeClass("partner-type-active");
//   $("#insurance-protocol").removeClass("show-partner-container");
//   $("#exchange-partners-tab").removeClass("partner-type-active");
//   $("#exchange-partners").removeClass("show-partner-container");
//   $("#platform-tab").addClass("partner-type-active");
//   $("#platform").addClass("show-partner-container");
// });

$("#infrastructure-tab").click(function(){
  $("#NFT-Marketplace-tab").removeClass("partner-type-active");
  $("#NFT-Marketplace").removeClass("show-partner-container");
  $("#liquidity-platform-tab").removeClass("partner-type-active");
  $("#liquidity-platform").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#integration-tab").removeClass("partner-type-active");
  $("#integration").removeClass("show-partner-container");
  $("#insurance-protocol-tab").removeClass("partner-type-active");
  $("#insurance-protocol").removeClass("show-partner-container");
  $("#exchange-partners-tab").removeClass("partner-type-active");
  $("#exchange-partners").removeClass("show-partner-container");
  $("#infrastructure-tab").addClass("partner-type-active");
  $("#infrastructure").addClass("show-partner-container");
});

$("#integration-tab").click(function(){
  $("#NFT-Marketplace-tab").removeClass("partner-type-active");
  $("#NFT-Marketplace").removeClass("show-partner-container");
  $("#liquidity-platform-tab").removeClass("partner-type-active");
  $("#liquidity-platform").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#infrastructure-tab").removeClass("partner-type-active");
  $("#infrastructure").removeClass("show-partner-container");
  $("#insurance-protocol-tab").removeClass("partner-type-active");
  $("#insurance-protocol").removeClass("show-partner-container");
  $("#exchange-partners-tab").removeClass("partner-type-active");
  $("#exchange-partners").removeClass("show-partner-container");
  $("#integration-tab").addClass("partner-type-active");
  $("#integration").addClass("show-partner-container");
});

$("#insurance-protocol-tab").click(function(){
  $("#NFT-Marketplace-tab").removeClass("partner-type-active");
  $("#NFT-Marketplace").removeClass("show-partner-container");
  $("#liquidity-platform-tab").removeClass("partner-type-active");
  $("#liquidity-platform").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#infrastructure-tab").removeClass("partner-type-active");
  $("#infrastructure").removeClass("show-partner-container");
  $("#integration-tab").removeClass("partner-type-active");
  $("#integration").removeClass("show-partner-container");
  $("#exchange-partners-tab").removeClass("partner-type-active");
  $("#exchange-partners").removeClass("show-partner-container");
  $("#insurance-protocol-tab").addClass("partner-type-active");
  $("#insurance-protocol").addClass("show-partner-container");
});


$("#exchange-partners-tab").click(function(){
  $("#NFT-Marketplace-tab").removeClass("partner-type-active");
  $("#NFT-Marketplace").removeClass("show-partner-container");
  $("#liquidity-platform-tab").removeClass("partner-type-active");
  $("#liquidity-platform").removeClass("show-partner-container");
  $("#platform-tab").removeClass("partner-type-active");
  $("#platform").removeClass("show-partner-container");
  $("#infrastructure-tab").removeClass("partner-type-active");
  $("#infrastructure").removeClass("show-partner-container");
  $("#integration-tab").removeClass("partner-type-active");
  $("#integration").removeClass("show-partner-container");
  $("#insurance-protocol-tab").removeClass("partner-type-active");
  $("#insurance-protocol").removeClass("show-partner-container");
  $("#exchange-partners-tab").addClass("partner-type-active");
  $("#exchange-partners").addClass("show-partner-container");
});