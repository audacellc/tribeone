const data = [
  {
    polkaDotImg : "./../images/diagram-icon/polkadot.png",
    polkaDotDescrreption : "Being Built on next generation blockchain solution based on Web3.0"
  },
  {
    lowCollateralLoan : "./../images/diagram-icon/collateral loans.png",
    lowCollateralLoanDescrreption : "Instant loans & approvals without any KYC"
  },
  {
    deFiProtocol : "./../images/diagram-icon/defi-protocol.png",
    deFiProtocolDescrreption : "First DeFi protocol with an inbuilt financial actuary (RAROC- Risk Adjusted Return on Crypto) to determine the rating of borrowers"
  },
  {
    microloans : "./../images/diagram-icon/micro-financers-loan.png",
    microloansDescrreption : "First one to introduce uncollateralized tailor-made loans for micro financiers"
  },
  {
    deFieInsurance : "./../images/diagram-icon/defi-insurance.png",
    deFieInsuranceDescrreption : "First platform to provide a DeFi Insurance with a two layered safety net against loan liquidation & defaults"
  },
  {
    savingPlan : "./../images/diagram-icon/nuu-trust.png",
    savingPlanDescrreption : "Pioneer platform to access Zero collateral loans for consumers (B2C) and microfinanciers (B2B)"
  },
  {
    NFT : "./../images/diagram-icon/nft.png",
    NFTDescrreption : "First platform to provide lending and borrowing against NFT and the option of investing in NFT"
  },
  {
    mobile : "./../images/diagram-icon/mobile-icon.svg",
    mobileDescreption : "Mobile App being developed with the latest front end technology"
  }
]


// polkadot
$("#poka_svg").mouseover(function(){
  $(".descreption-img").attr("src", data[0].polkaDotImg)
  $(".binance-img").addClass("show-binance-img");
  $(".diagram-descreption").html(data[0].polkaDotDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#poka_svg").click(function(){
  $(".descreption-img").attr("src", data[0].polkaDotImg)
  $(".binance-img").addClass("show-binance-img");
  $(".diagram-descreption").html(data[0].polkaDotDescrreption);
  $(".descreption-container").addClass("show-descreption");
})
$("#poka_svg").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
  $(".binance-img").removeClass("show-binance-img");
});

// instant loan
$("#crypto_haka").mouseover(function(){
  $(".descreption-img").attr("src", data[1].lowCollateralLoan)
  $(".diagram-descreption").html(data[1].lowCollateralLoanDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#crypto_haka").click(function(){
  $(".descreption-img").attr("src", data[1].lowCollateralLoan)
  $(".diagram-descreption").html(data[1].lowCollateralLoanDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#crypto_haka").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});

// deFiProtocol
$("#_ëîé_1").mouseover(function(){
  $(".descreption-img").attr("src", data[2].deFiProtocol)
  $(".diagram-descreption").html(data[2].deFiProtocolDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#_ëîé_1").click(function(){
  $(".descreption-img").attr("src", data[2].deFiProtocol)
  $(".diagram-descreption").html(data[2].deFiProtocolDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#_ëîé_1").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});

// sme loan
$(".sme-loan").mouseover(function(){
  $(".descreption-img").attr("src", data[3].microloans)
  $(".diagram-descreption").html(data[3].microloansDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$(".sme-loan").click(function(){
  $(".descreption-img").attr("src", data[3].microloans)
  $(".diagram-descreption").html(data[3].microloansDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$(".sme-loan").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});

// polkacover
$("#insurance").mouseover(function(){
  $(".descreption-img").attr("src", data[4].deFieInsurance)
  $(".diagram-descreption").html(data[4].deFieInsuranceDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#insurance").click(function(){
  $(".descreption-img").attr("src", data[4].deFieInsurance)
  $(".diagram-descreption").html(data[4].deFieInsuranceDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#insurance").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});

// savingplan
$(".file").mouseover(function(){
  $(".descreption-img").attr("src", data[5].savingPlan)
  $(".diagram-descreption").html(data[5].savingPlanDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$(".file").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});
$(".file").on("click",function(){
  $(".descreption-img").attr("src", data[5].savingPlan)
  $(".diagram-descreption").html(data[5].savingPlanDescrreption);
  $(".descreption-container").addClass("show-descreption");
});

// nft
$(".nft").mouseover(function(){
  $(".descreption-img").attr("src", data[6].NFT)
  $(".diagram-descreption").html(data[6].NFTDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$(".nft").click(function(){
  $(".descreption-img").attr("src", data[6].NFT)
  $(".diagram-descreption").html(data[6].NFTDescrreption);
  $(".descreption-container").addClass("show-descreption");
});
$(".nft").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});


// mobile
$("#mobile-icon").mouseover(function(){
  $(".descreption-img").attr("src", data[7].mobile)
  $(".diagram-descreption").html(data[7].mobileDescreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#mobile-icon").click(function(){
  $(".descreption-img").attr("src", data[7].mobile)
  $(".diagram-descreption").html(data[7].mobileDescreption);
  $(".descreption-container").addClass("show-descreption");
});
$("#mobile-icon").mouseout(function(){
  $(".descreption-container").removeClass("show-descreption");
});